/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.cli.cmdline.app;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.List;
import java.util.Map;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;

import cern.plcverif.cli.cmdline.CmdlineAppMain;

public class Application implements IApplication {

	@Override
	public Object start(IApplicationContext context) throws Exception {
		// Fetching command line arguments
		final Map<?, ?> args = context.getArguments();
		final String[] appArgs = (String[]) args.get("application.args");
		List<String> argumentList = Arrays.asList(appArgs);
		
		// Get version number
		Object versionObj = null;
		try {
			Dictionary<?, ?> bundleHeaders = Activator.getContext().getBundle().getHeaders();
			versionObj = bundleHeaders.get("Bundle-version");
		} catch (Exception ex) {
			// On any exception the version number will remain 'unknown'.
			// If it is not possible to fetch the version number, we should still be able to run. 
		}
		String versionStr = versionObj == null ? "unknown" : versionObj.toString();
		
		// Determining the program directory
		Path programDirectory;
		try {
			programDirectory = Paths.get(org.eclipse.core.runtime.Platform.getInstallLocation().getURL().toURI()).toAbsolutePath();
		} catch (URISyntaxException e) {
			System.err.println("Unable to determine the location of PLCverif: " + org.eclipse.core.runtime.Platform.getInstallLocation().getURL().toString());
			e.printStackTrace();
			return IApplication.EXIT_OK;
		}
		
		// Run application
		CmdlineAppMain.cmdlineMain(argumentList, programDirectory, versionStr);
		
		Thread.sleep(100); // workaround for Gogo issue, see https://issues.apache.org/jira/browse/FELIX-3331
		
		return IApplication.EXIT_OK;
	}

	@Override
	public void stop() {
		// Nothing to do
	}

}

# cern.plcverif.cli

## PLCverif CLI executables 

| Platform | Release |
| --- | --- |
| Windows | [zip](https://plcverif-oss.gitlab.io/cern.plcverif.cli/releases/cern.plcverif.cli.cmdline.app.product-win32.win32.x86_64.zip)  |
| Linux | [tar.gz](https://plcverif-oss.gitlab.io/cern.plcverif.cli/releases/cern.plcverif.cli.cmdline.app.product-linux.gtk.x86_64.tar.gz) |

## PLCverif documentation
| Type | PDF | HTML |
| ---- | --- | ---- |
| Full developer documentation | [PDF](https://plcverif-oss.gitlab.io/plcverif-docs/devdocs.pdf) | [HTML](https://plcverif-oss.gitlab.io/plcverif-docs/) |
| User documentation | [PDF](https://plcverif-oss.gitlab.io/plcverif-docs/userdocs.pdf) | [HTML](https://plcverif-oss.gitlab.io/plcverif-docs/userdocs/) | 

## Other links specific to cern.plcverif.gui
[p2 repository](https://plcverif-oss.gitlab.io/cern.plcverif.cli/p2/p2.index)

[Javadoc](https://plcverif-oss.gitlab.io/cern.plcverif.cli/javadoc)

[Coverage report](https://plcverif-oss.gitlab.io/cern.plcverif.cli/jacoco)

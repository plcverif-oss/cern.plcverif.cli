/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.cli.cmdline;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.logging.PlatformLogger;
import cern.plcverif.base.common.progress.NullProgressReporter;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.interfaces.IBasicJob;
import cern.plcverif.base.interfaces.data.BasicJobResult;
import cern.plcverif.base.interfaces.data.PlatformConfig;
import cern.plcverif.base.interfaces.data.result.OutputFileToSave;
import cern.plcverif.base.platform.Platform;

public final class CmdlineAppMain {
	private CmdlineAppMain() {
		// Only static methods.
	}
	
	public static void cmdlineMain(List<String> argumentList, Path programDirectory, String version) {
		Preconditions.checkNotNull(argumentList, "argumentList");
		Preconditions.checkNotNull(programDirectory, "programDirectory");

		System.out.println("PLCverif    by CERN BE-ICS-TMA, (c) 2013-2024");
		System.out.println("   CLI version: " + version);
		System.out.println("--------------------------------------------");
		System.out.println("Arguments: " + String.join("  ", argumentList));
		System.out.println("Program directory: " + programDirectory);

		// Create platform
		PlatformConfig platformConfig = new PlatformConfig(programDirectory);
		Platform platform = Platform.createPlatform(platformConfig);

		if (argumentList.isEmpty() || argumentList.contains("-help") || argumentList.contains("-?")) {
			if (argumentList.isEmpty()) {
				System.err.println("At least one argument is mandatory.");
			}
			printHelp(platform);
			return;
		}

		Preconditions.checkState(!argumentList.isEmpty());

		// Try to handle the first cmdline argument as a settings file
		SettingsElement effectiveSettings = null;
		boolean firstArgIsSettingsFile = false;

		if (!argumentList.get(0).startsWith("-")) {
			String firstArg = argumentList.get(0);
			File input = new File(firstArg);
			if (input.exists()) {
				try {
					PlatformLogger.logInfo(
							String.format("Parsing config file '%s'...%n", input.getAbsoluteFile().toString()));
					List<String> config = Files.readAllLines(Paths.get(firstArg));
					effectiveSettings = SettingsSerializer.parseSettingsFromFile(config);
					firstArgIsSettingsFile = true;
				} catch (IOException | SettingsParserException e) {
					PlatformLogger.logError("Unable to load the given configuration file. " + e.getMessage());
					return;
				}
			}
		}

		// Try to parse all (or remaining) parameters as settings
		PlatformLogger.logInfo(
				String.format("Config file '%s' not found, parsing arguments as parameters...%n", argumentList.get(0)));
		List<String> argumentsToParse;
		if (firstArgIsSettingsFile && argumentList.size() >= 2) {
			argumentsToParse = argumentList.subList(1, argumentList.size());
		} else if (firstArgIsSettingsFile && argumentList.size() <= 1) {
			argumentsToParse = null;
		} else {
			argumentsToParse = argumentList;
		}

		if (argumentsToParse != null) {
			try {
				SettingsElement settingsFromArguments = SettingsSerializer.parseSettingsFromCommandArgs(argumentsToParse);

				if (effectiveSettings == null) {
					effectiveSettings = settingsFromArguments;
				} else {
					effectiveSettings.overrideWith(settingsFromArguments);
				}
			} catch (SettingsParserException e) {
				PlatformLogger.logError("Unable to parse the given parameters. " + e.getMessage());
				return;
			}
		}

		// If nothing worked, give up
		if (effectiveSettings == null) {
			PlatformLogger.logError("Unable to load the given settings.");
			return;
		}

		// Execute job
		try {
			IBasicJob job = platform.parseJob(effectiveSettings);
			BasicJobResult result = job.execute(NullProgressReporter.getInstance());
			Preconditions.checkState(result != null, "The result of the executed job is null.");

			// Write output artifacts
			if (!result.getOutputFilesToSave().isEmpty()) {
				Path outputDir = result.getOutputDirectoryOrCreateTemp();
				Preconditions.checkNotNull(outputDir, "Not possible to write outputs: the result of the job does not specify the output directory.");

				for (OutputFileToSave file : result.getOutputFilesToSave()) {
					// file.getFilename() and file.getContent() are never null
					Path outputFile = outputDir.resolve(file.getFilename()).toAbsolutePath();
					IoUtils.writeAllContent(outputFile, file.getContent());
					PlatformLogger.logDebug("Output written to " + outputFile);
				}
			}
		} catch (SettingsParserException ex) {
			PlatformLogger.logError("Unable to parse the given settings. " + ex.getMessage(), ex);
		} catch (PlcverifPlatformException ex) {
			PlatformLogger.logError("PLCverif error. " + ex.getMessage(), ex);
		} catch (IOException ex) {
			PlatformLogger.logError("Unable to write to file. " + ex.getMessage(), ex);
		} catch (NullPointerException ex) {
			PlatformLogger.logError(String.format("Unexpected exception (NPE). %s Check the log file for more details.", ex.getMessage()), ex);
		} catch (IllegalStateException ex) {
			PlatformLogger.logError(String.format("Unexpected exception (illegal state). %s Check the log file for more details.",  ex.getMessage()), ex);
		} catch (Exception ex) {
			PlatformLogger.logError(String.format("Unexpected exception (%s). ", ex.getClass().getSimpleName()) + ex.getMessage(), ex);
		}
	}

	private static void printHelp(Platform platform) {
		System.out.println("PERMITTED COMMAND LINE PARAMETERS:");
		platform.printSettingsHelp();
	}
}

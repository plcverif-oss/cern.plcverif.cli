package cern.plcverif.cli.cmdline.tests;

import java.io.PrintWriter;

import org.junit.Ignore;
import org.junit.Test;

import cern.plcverif.base.common.settings.help.MarkdownHelpWriter;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.interfaces.data.PlatformConfig;
import cern.plcverif.base.platform.Platform;

public class PrintHelp {
	@Test
	@Ignore
	public void printMarkdownHelp() {
		Platform platform = Platform.createPlatform(new PlatformConfig());
		try (PrintWriter printWriter = new PrintWriter(System.out)) {
			SettingsHelp help = new SettingsHelp(new MarkdownHelpWriter(printWriter, 2));
			platform.fillSettingsHelp(help);
		}
	}
}
